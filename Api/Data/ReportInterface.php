<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Api\Data;

/**
 * Interface ReportInterface
 * @package Magebit\NotFound\Api\Data
 */
interface ReportInterface
{
    const REPORT_ID = 'report_id';
    const STORE_ID = 'store_id';
    const REQUEST_URL = 'request_url';
    const COUNT = 'count';
    const STATUS = 'status';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $value
     * @return mixed
     */
    public function setId($value);

    /**
     * @return mixed
     */
    public function getStoreId();

    /**
     * @param $value
     * @return mixed
     */
    public function setStoreId($value);

    /**
     * @return mixed
     */
    public function getRequestUrl();

    /**
     * @param $value
     * @return mixed
     */
    public function setRequestUrl($value);

    /**
     * @return mixed
     */
    public function getCount();

    /**
     * @param $value
     * @return mixed
     */
    public function setCount($value);

    /**
     * @return mixed
     */
    public function getStatus();

    /**
     * @param $value
     * @return mixed
     */
    public function setStatus($value);

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param $value
     * @return mixed
     */
    public function setCreatedAt($value);

    /**
     * @return mixed
     */
    public function getUpdatedAt();

    /**
     * @param $value
     * @return mixed
     */
    public function setUpdatedAt($value);
}