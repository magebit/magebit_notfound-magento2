<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Plugin\Adminhtml;

use Magebit\NotFound\Model\ResourceModel\Report;
use Magebit\NotFound\Model\ReportFactory;
use Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Save;

/**
 * Class UrlRewriteRegister
 * @package Magebit\NotFound\Plugin\Adminhtml
 */
class UrlRewriteRegister
{
    /**
     * @var ReportFactory
     */
    protected $_reportFactory;

    /**
     * @var Report
     */
    protected $_report;

    /**
     * UrlRewriteRegister constructor.
     * @param ReportFactory $reportFactory
     * @param Report $report
     */
    public function __construct(ReportFactory $reportFactory, Report $report)
    {
        $this->_reportFactory = $reportFactory;
        $this->_report = $report;
    }

    /**
     * @param Save $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(Save $subject, $result)
    {
        if($reportId = $subject->getRequest()->getParam('requestid')){
            /** @var \Magebit\NotFound\Model\Report $report */
            $report = $this->_reportFactory->create()->load($reportId);
            $report->setStatus(1);
            $this->_report->save($report);
        }
        return $result;
    }
}