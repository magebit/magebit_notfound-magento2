<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Plugin\Adminhtml;

use Magebit\NotFound\Model\ResourceModel\Report;
use Magebit\NotFound\Model\ReportFactory;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Magento\UrlRewrite\Controller\Adminhtml\Url\Rewrite\Delete;

/**
 * Class UrlRewriteUnregister
 * @package Magebit\NotFound\Plugin\Adminhtml
 */
class UrlRewriteUnregister
{
    /**
     * @var ReportFactory
     */
    protected $_reportFactory;

    /**
     * @var Report
     */
    protected $_report;

    /**
     * @var UrlRewriteFactory
     */
    protected $_urlRewriteFactory;

    /**
     * UrlRewriteUnregister constructor.
     * @param ReportFactory $reportFactory
     * @param Report $report
     * @param UrlRewriteFactory $urlRewriteFactory
     */
    public function __construct(ReportFactory $reportFactory, Report $report, UrlRewriteFactory $urlRewriteFactory)
    {
        $this->_reportFactory = $reportFactory;
        $this->_report = $report;
        $this->_urlRewriteFactory = $urlRewriteFactory;
    }

    /**
     * @param Delete $subject
     */
    public function beforeExecute(Delete $subject)
    {
        /** @var \Magento\UrlRewrite\Model\UrlRewrite $rewrite */
        $rewrite = $this->_urlRewriteFactory->create()->load($subject->getRequest()->getParam('id'));

        if($reports = $this->_reportFactory->create()->getCollection()->addFieldToFilter('request_url', $rewrite->getRequestPath())){
            foreach ($reports as $report){
                /** @var \Magebit\NotFound\Model\Report $report */
                $report->setStatus(0);
                $this->_report->save($report);
            }
        }
    }
}