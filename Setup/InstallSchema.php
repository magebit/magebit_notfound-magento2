<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Setup;

use Magento\Framework\Setup;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * @package Magebit\NotFound\Setup
 */
class InstallSchema implements Setup\InstallSchemaInterface
{
    public function install(Setup\SchemaSetupInterface $setup, Setup\ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('magebit_notFound_report')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('magebit_notFound_report')
            )
                ->addColumn(
                    'report_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Report ID'
                )
                ->addColumn(
                    'request_url',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => false],
                    'Requested Url'
                )
                ->addColumn(
                    'count',
                    Table::TYPE_INTEGER,
                    null,
                    ['default' => 1],
                    'Times Requested'
                )->addColumn(
                    'created_at',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'First Time Requested'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_DATETIME,
                    null,
                    [],
                    'Last Time Requested'
                )
                ->setComment('Not Found Reports Table');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}