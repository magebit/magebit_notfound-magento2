<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Controller\Adminhtml\Report;

use Magebit\NotFound\Controller\Adminhtml\Report;

/**
 * Class Save
 * @package Magebit\NotFound\Controller\Adminhtml\Report
 */
class Save extends Report
{
    /**
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $formData = $this->getRequest()->getParam('report');
        $count = 0;
        try{
            foreach ($formData['selected_urls'] as $reportId){
                /** @var \Magebit\NotFound\Model\Report $report */
                /** @var \Magento\UrlRewrite\Model\UrlRewrite $rewrite */

                if($report = $this->_reportFactory->create()->load($reportId)){
                    $rewrite = $this->createRewrite();
                    $rewrite->setRequestPath($report->getRequestUrl());
                    $rewrite->setStoreId($report->getStoreId());
                    $this->_urlRewrite->save($rewrite);
                    $report->setStatus(1);
                    $this->_report->save($report);
                    $count++;
                }
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 url(s) have been redirected.', $count));
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving URL Rewrite.'));
        }

        return $this->_redirect('*/*/index');
    }

    /**
     * Loads rewrite model and sets values
     * @return mixed
     */
    public function createRewrite(){
        $formData = $this->getRequest()->getParam('report');
        /** @var \Magento\UrlRewrite\Model\UrlRewrite $rewrite */
        $rewrite = $this->_urlRewriteFactory->create();
        $rewrite->setEntityType('custom');
        $rewrite->setEntityId(0);
        $rewrite->setTargetPath($formData['target_path']);
        $rewrite->setDescription($formData['description']);
        $rewrite->setRedirectType($formData['redirect_type']);

        return $rewrite;
    }
}