<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Controller\Adminhtml\Report;

use Magebit\NotFound\Controller\Adminhtml\Report;

/**
 * Class MassDelete
 * @package Magebit\NotFound\Controller\Adminhtml\Report
 */
class MassDelete extends Report
{
    /**
     * Deletes selected reports
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('ids');
        $collection = $this->_reportFactory->create()->getCollection()
            ->addFieldToFilter('report_id', $ids);
        $count = $collection->getSize();

        $collection->walk('delete');

        $this->messageManager->addSuccessMessage(__('A total of %1 report(s) have been deleted.', $count));

        return $this->_redirect('*/*/index');
    }
}