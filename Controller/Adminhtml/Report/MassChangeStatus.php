<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Controller\Adminhtml\Report;

use Magebit\NotFound\Controller\Adminhtml\Report;

/**
 * Class MassChangeStatus
 * @package Magebit\NotFound\Controller\Adminhtml\Report
 */
class MassChangeStatus extends Report
{
    /**
     * Changes report status
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $disabled = $enabled = 0;

        foreach ($collection as $item) {
            /** @var \Magebit\NotFound\Model\Report $item */
            $item->setStatus(!$item->getStatus());
            $this->_report->save($item);

            if($item->getStatus()){
                $disabled++;
            } else {
                $enabled++;
            }
        }

        $this->messageManager->addSuccessMessage(__(
            'A total of %1 report(s) status has been changed to Redirected
            and a total of %2 report(s) status has beend changed to Undirected.', $enabled, $disabled));

        return $this->_redirect('*/*/index');
    }
}