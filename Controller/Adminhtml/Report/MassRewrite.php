<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Controller\Adminhtml\Report;

use Magebit\NotFound\Controller\Adminhtml\Report;

/**
 * Class MassRewrite
 * @package Magebit\NotFound\Controller\Adminhtml\Report
 */
class MassRewrite extends Report
{
    /**
     * Loads mass url rewrite form
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('ids');
        $this->_coreRegistry->register('selected_urls', $ids);

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Magebit_NotFound::magebit');
        $resultPage->getConfig()->getTitle()->prepend(__('Mass Rewrite'));

        return $resultPage;
    }
}