<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Magebit\NotFound\Controller\Adminhtml;

use Magebit\Mysport\Model\App\Url;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\Model\Auth\Session;
use Magento\Ui\Component\MassAction\Filter;
use Magebit\NotFound\Model\ReportFactory;
use Magebit\NotFound\Model\ResourceModel\Report as ReportModel;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewrite;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Magebit\NotFound\Model\ResourceModel\Report\CollectionFactory;

/**
 * Class Report
 * @package Magebit\NotFound\Controller\Adminhtml
 */
abstract class Report extends Action
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var ReportFactory
     */
    protected $_reportFactory;

    /**
     * @var ReportModel
     */
    protected $_report;

    /**
     * @var Session
     */
    protected $_admin;

    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var UrlRewriteFactory
     */
    protected $_urlRewriteFactory;

    /**
     * @var UrlRewrite
     */
    protected $_urlRewrite;

    /**
     * Report constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     * @param ReportFactory $reportFactory
     * @param ReportModel $report
     * @param Session $admin
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewrite $urlRewrite
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        ReportFactory $reportFactory,
        ReportModel $report,
        Session $admin,
        Filter $filter,
        CollectionFactory $collectionFactory,
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewrite $urlRewrite
    ) {
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_reportFactory = $reportFactory;
        $this->_report = $report;
        $this->_admin = $admin;
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_urlRewriteFactory = $urlRewriteFactory;
        $this->_urlRewrite = $urlRewrite;
    }
}