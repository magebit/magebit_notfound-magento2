<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Magebit\NotFound\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

/**
 * Class Report
 * @package Magebit\NotFound\Block\Adminhtml
 */
class Report extends Container
{
    /**
     * @return Container
     */
    protected function _prepareLayout()
    {
        $this->removeButton('add');
        return parent::_prepareLayout();
    }
}