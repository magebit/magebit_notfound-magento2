<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit;

use Magento\Backend\Block\Widget\Form\Generic;

/**
 * Class Form
 * @package Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit
 */
class Form extends Generic
{
    /**
     * Prepares the layout
     * @return mixed
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id'    => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
            ]
        );
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}