<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

/**
 * Class Tabs
 * @package Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit
 */
class Tabs extends WidgetTabs
{
    /**
     *  Tabs constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('massRewrite_edit_tab');
        $this->setDestElementId('edit_form');
    }

    /**
     * Defines which tabs to load
     *
     * @return $this
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'url_selected',
            [
                'label' => __('Selected URLs'),
                'title' => __('Selected URLs'),
                'content' => $this->getLayout()->createBlock(
                    \Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit\Tabs\Urls::class
                )->toHtml(),
                'active' => false
            ]
        );

        $this->addTab(
            'url_rewrite',
            [
                'label' => __('URL Rewrite'),
                'title' => __('URL Rewrite'),
                'content' => $this->getLayout()->createBlock(
                    \Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit\Tabs\Rewrite::class
                )->toHtml(),
                'active' => true
            ]
        );
        return parent::_beforeToHtml();
    }
}