<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit\Tabs;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Model\Auth\Session;
use Magebit\NotFound\Model\Report\OptionProvider;

/**
 * Class Urls
 * @package Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit\Tabs
 */
class Urls extends Generic
{
    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * @var OptionProvider
     */
    protected $_optionProvider;

    /**
     * Urls constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Session $authSession
     * @param OptionProvider $optionProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Session $authSession,
        OptionProvider $optionProvider,
        array $data = []
    )
    {
        $this->_authSession = $authSession;
        $this->_optionProvider = $optionProvider;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepares form
     *
     * @return $this
     */
    public function _prepareForm()
    {
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('report_');
        $form->setFieldNameSuffix('report');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Selected URLs')]);

        $fieldset->addField(
            'selected_urls',
            'multiselect',
            [
                'name' => 'selected_urls[]',
                'label' => __('Undirected URLs'),
                'title' => __('URLs'),
                'required' => true,
                'disabled' => false,
                'values' => $this->_optionProvider->toOptionArray(),
                'value' => $this->_coreRegistry->registry('selected_urls')
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
