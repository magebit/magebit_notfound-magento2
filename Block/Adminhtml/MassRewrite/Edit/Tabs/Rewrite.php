<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit\Tabs;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Model\Auth\Session;
use Magento\UrlRewrite\Model\OptionProvider;
use Magento\Store\Model\System\Store;

/**
 * Class Rewrite
 * @package Magebit\NotFound\Block\Adminhtml\MassRewrite\Edit\Tabs
 */
class Rewrite extends Generic
{
    /**
     * @var Session
     */
    protected $_authSession;

    /**
     * @var OptionProvider
     */
    protected $_optionProvider;

    /**
     * @var Store
     */
    protected $_systemStore;

    /**
     * Rewrite constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Session $authSession
     * @param OptionProvider $optionProvider
     * @param Store $store
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Session $authSession,
        OptionProvider $optionProvider,
        Store $store,
        array $data = []
    ) {
        $this->_authSession = $authSession;
        $this->_optionProvider = $optionProvider;
        $this->_systemStore = $store;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepares form
     *
     * @return Generic
     */
    public function _prepareForm()
    {
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('report_');
        $form->setFieldNameSuffix('report');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('URL Rewrite Information')]);

        $fieldset->addField(
            'target_path',
            'text',
            [
                'label' => __('Target Path'),
                'title' => __('Target Path'),
                'name' => 'target_path',
                'required' => true,
                'disabled' => false
            ]
        );

        $fieldset->addField(
            'redirect_type',
            'select',
            [
                'label' => __('Redirect Type'),
                'title' => __('Redirect Type'),
                'name' => 'redirect_type',
                'options' => $this->_optionProvider->toOptionArray()
            ]
        );

        $fieldset->addField(
            'description',
            'textarea',
            [
                'label' => __('Description'),
                'title' => __('Description'),
                'name' => 'description',
                'cols' => 20,
                'rows' => 5,
                'wrap' => 'soft'
            ]
        );

        $this->setForm($form);

        return parent::_prepareForm();
    }
}
