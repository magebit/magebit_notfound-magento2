<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Magebit\NotFound\Block\Adminhtml\Rewrite;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Magento\UrlRewrite\Block\Edit\Form as UrlRewriteForm;
use Magebit\NotFound\Model\ReportFactory;
use Magento\UrlRewrite\Model\OptionProvider;
use Magento\UrlRewrite\Model\UrlRewriteFactory;

/**
 * Class Form
 * @package Magebit\NotFound\Block
 */
class Form extends UrlRewriteForm
{
    /**
     * @var ReportFactory
     */
    protected $_reportFactory;

    /**
     * Form constructor
     *
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param OptionProvider $optionProvider
     * @param UrlRewriteFactory $rewriteFactory
     * @param Store $systemStore
     * @param Data $adminhtmlData
     * @param ReportFactory $reportFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        OptionProvider $optionProvider,
        UrlRewriteFactory $rewriteFactory,
        Store $systemStore,
        Data $adminhtmlData,
        ReportFactory $reportFactory,
        array $data = []
    ) {
        $this->_reportFactory = $reportFactory;
        parent::__construct(
            $context,
            $registry,
            $formFactory,
            $optionProvider,
            $rewriteFactory,
            $systemStore,
            $adminhtmlData,
            $data
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareForm()
    {
        $this->_initFormValues();

        // Prepare form
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'use_container' => true, 'method' => 'post']]
        );

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('URL Rewrite Information')]);

        $fieldset->addField(
            'entity_type',
            'hidden',
            [
                'name' => 'entity_type',
                'value' => $this->_formValues['entity_type']
            ]
        );

        $fieldset->addField(
            'entity_id',
            'hidden',
            [
                'name' => 'entity_id',
                'value' => $this->_formValues['entity_id']
            ]
        );

        $fieldset->addField(
            'requestid',
            'hidden',
            [
                'name' => 'requestid',
                'value' => $this->getRequest()->getParam('requestid')
            ]
        );

        $this->_prepareStoreElement($fieldset);

        $fieldset->addField(
            'request_path',
            'text',
            [
                'label' => __('Request Path'),
                'title' => __('Request Path'),
                'name' => 'request_path',
                'required' => true,
                'value' => $this->_formValues['request_path']
            ]
        );

        $fieldset->addField(
            'target_path',
            'text',
            [
                'label' => __('Target Path'),
                'title' => __('Target Path'),
                'name' => 'target_path',
                'required' => true,
                'disabled' => false,
                'value' => $this->_formValues['target_path']
            ]
        );

        $fieldset->addField(
            'redirect_type',
            'select',
            [
                'label' => __('Redirect Type'),
                'title' => __('Redirect Type'),
                'name' => 'redirect_type',
                'options' => $this->optionProvider->toOptionArray(),
                'value' => $this->_formValues['redirect_type']
            ]
        );

        $fieldset->addField(
            'description',
            'textarea',
            [
                'label' => __('Description'),
                'title' => __('Description'),
                'name' => 'description',
                'cols' => 20,
                'rows' => 5,
                'value' => $this->_formValues['description'],
                'wrap' => 'soft'
            ]
        );

        $this->setForm($form);
        $this->_formPostInit($form);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function _initFormValues()
    {
        if($reportId = $this->getRequest()->getParam('requestid')){
            $model = $this->_reportFactory->create();
            $model->load($reportId);
            $this->_formValues = [
                'store_id' => $model->getStoreId(),
                'entity_type' => 'custom',
                'entity_id' => 0,
                'request_path' => $model->getRequestUrl(),
                'target_path' => '',
                'redirect_type' => '',
                'description' => '',
            ];
        }
        else{
            $model = $this->_getModel();
            $this->_formValues = [
                'store_id' => $model->getStoreId(),
                'entity_type' => $model->getEntityType(),
                'entity_id' => $model->getEntityId(),
                'request_path' => $model->getRequestPath(),
                'target_path' => $model->getTargetPath(),
                'redirect_type' => $model->getRedirectType(),
                'description' => $model->getDescription(),
            ];
        }

        $sessionData = $this->_getSessionData();
        if ($sessionData) {
            foreach (array_keys($this->_formValues) as $key) {
                if (isset($sessionData[$key])) {
                    $this->_formValues[$key] = $sessionData[$key];
                }
            }
        }
    }
}