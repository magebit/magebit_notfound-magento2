<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Observer;

use Magebit\NotFound\Model\ResourceModel\Report;
use Magento\Framework\Event\Observer;
use Magebit\NotFound\Model\ReportFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class NoRoute
 * @package Magebit\NotFound\Observer
 */
class NoRoute implements ObserverInterface
{
    /**
     * @var UrlInterface
     */
    protected $_urlInterface;

    /**
     * @var ReportFactory
     */
    protected $_reportFactory;

    /**
     * @var Report
     */
    protected $_report;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * NoRoute constructor
     *
     * @param ReportFactory $reportFactory
     * @param Report $report
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $urlInterface
     */
    public function __construct(
        ReportFactory $reportFactory,
        Report $report,
        StoreManagerInterface $storeManager,
        UrlInterface $urlInterface
    )
    {
        $this->_urlInterface = $urlInterface;
        $this->_reportFactory = $reportFactory;
        $this->_report = $report;
        $this->_storeManager = $storeManager;
    }

    /**
     * Registers no route
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $store = $this->_storeManager->getStore(null);
        $storeId = $store->getStoreId();
        $storeCode = $store->getCode().'/';

        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $observer->getData('request');
        $url = trim($request->getRequestUri(), '/');
        if($this->hasStoreCode($url, $storeCode)){
            $url = preg_replace('#'.$storeCode.'#', '', $url, 1);
        }

        /** @var \Magebit\NotFound\Model\Report $_report */
        $_report = $this->_reportFactory->create();
        /** @var \Magebit\NotFound\Model\Report $report */
        $report = $_report->getCollection()
            ->addFieldToFilter('request_url', $url)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem();

        if($report->getId()){
            $report->setCount($report->getCount() + 1);
        } else{
            $report->setStoreId($storeId);
            $report->setRequestUrl($url);
            $report->setCreatedAt(time());
        }

        $report->setUpdatedAt(time());
        $this->_report->save($report);
    }

    public function hasStoreCode($url, $storeCode){
        for ($i = 0; $i < strlen($storeCode); $i++){
            if ($url[$i] != $storeCode[$i]){
                return false;
            }
        }
        return true;
    }
}
