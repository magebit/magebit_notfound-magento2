<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Magebit\NotFound\Model\ResourceModel\Report;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Magebit\NotFound\Model\ResourceModel\Report
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'report_id';

    /**
     * Collection constructor
     */
    protected function _construct()
    {
        $this->_init('Magebit\NotFound\Model\Report', 'Magebit\NotFound\Model\ResourceModel\Report');
    }
}
