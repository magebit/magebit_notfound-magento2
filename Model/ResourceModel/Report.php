<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Magebit\NotFound\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Report
 * @package Magebit\NotFound\Model\ResourceModel
 */
class Report extends AbstractDb
{
    /**
     * Report constructor
     */
    protected function _construct()
    {
        $this->_init('magebit_notFound_report', 'report_id');
    }
}