<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Model;
use Magebit\NotFound\Api\Data\ReportInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Report
 * @package Magebit\NotFound\Model
 */
class Report extends AbstractModel implements ReportInterface
{
    /**
     * Report constructor
     */
    protected function _construct()
    {
        $this->_init('Magebit\NotFound\Model\ResourceModel\Report');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->getData(self::REPORT_ID);
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setId($value)
    {
        return $this->setData(self::REPORT_ID, $value);
    }

    /**
     * @return mixed
     */
    public function getRequestUrl()
    {
        return $this->getData(self::REQUEST_URL);
    }

    /**
     * @param $value
     * @return $this
     */
    public function setRequestUrl($value)
    {
        return $this->setData(self::REQUEST_URL, $value);
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->getData(self::COUNT);
    }

    /**
     * @param $value
     * @return $this
     */
    public function setCount($value)
    {
        return $this->setData(self::COUNT, $value);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param $value
     * @return $this
     */
    public function setStatus($value)
    {
        return $this->setData(self::STATUS, $value);
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @param $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        return $this->setData(self::CREATED_AT, $value);
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * @param $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        return $this->setData(self::UPDATED_AT, $value);
    }

    /**
     * @return mixed
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setStoreId($value)
    {
        return $this->setData(self::STORE_ID, $value);
    }
}