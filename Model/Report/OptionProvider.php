<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Magebit\NotFound\Model\Report;

use Magento\Framework\Option\ArrayInterface;
use Magebit\NotFound\Model\ReportFactory;

/**
 * Class OptionProvider
 * @package Magebit\NotFound\Model\Report
 */
class OptionProvider implements ArrayInterface
{
    /**
     * @var ReportFactory
     */
    protected $_reportFactory;

    /**
     * OptionProvider constructor.
     * @param ReportFactory $reportFactory
     */
    public function __construct(ReportFactory $reportFactory)
    {
        $this->_reportFactory = $reportFactory;
    }

    /**
     * Loads all undirected urls
     * @return array
     */
    public function toOptionArray()
    {
        $reportCollection = $this->_reportFactory->create()->getCollection()
            ->addFieldToFilter('status', 0)
            ->addFieldToSelect('report_id')
            ->addFieldToSelect('request_url');

        $options = array();

        /** @var \Magebit\NotFound\Model\Report $value */
        foreach ($reportCollection as $key=>$value){
            $options[$key] = [
                'value' => $value->getId(),
                'label' => __($value->getRequestUrl())
            ];
        }

        return $options;
    }
}