<?php
/**
 * Magebit_NotFound
 *
 * @category     Magebit
 * @package      Magebit_NotFound
 * @author       Aigars Mūriņš <aigars.murins@magebit.com>
 * @copyright    Copyright (c) 2017 Magebit, Ltd.            (http://magebit.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Magebit\NotFound\Model\Report;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Status
 * @package Magebit\NotFound\Model\Report
 */
class Status implements ArrayInterface
{
    /**
     * Creates option array for report status
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 1, 'label' => __('Redirected')],
            ['value' => 0, 'label' => __('Undirected')]
        ];
    }
}